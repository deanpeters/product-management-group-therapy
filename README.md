# Product Management Group Therapy

A repository I'm creating in support of my upcoming podcast titled 'Product Management Group Therapy' ... where I will provide a cathartic sounding-board and marginal advice addressing the pain points of Product People of all titles and roles.